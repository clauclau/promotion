package com.example.promotion.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface PromotionRepositoryImpl extends PromotionRepository {
}
