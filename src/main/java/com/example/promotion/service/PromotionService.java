package com.example.promotion.service;

import com.example.promotion.model.Promotion;
import com.example.promotion.repository.PromotionRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PromotionService {
    @Autowired
    private PromotionRepositoryImpl promotionRepository;

    @Autowired
    private ClientService clientService;

    public Promotion save(Promotion promotion){
        return promotionRepository.save(promotion);
    }
    public List<Promotion> findAll(){
        return promotionRepository.findAll();
    }
}
